package cat.itb.chessclone;

import android.accounts.AbstractAccountAuthenticator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class JugarFragment extends Fragment {

    static List<Modo> modos;
    private AdapterJugar adapterJugar = null;
    private RecyclerView recyclerView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        modos = new ArrayList<>();
        modos.add(new Modo(R.drawable.tablero_1, R.drawable.modo_1, "Juega en línea", "10 min vs Aleatorio"));
        modos.add(new Modo(R.drawable.tablero_1, R.drawable.modo_1, "Jugar contra PC", "Aron"));
        modos.add(new Modo(R.drawable.tablero_1, R.drawable.modo_1, "Resuelve Problemas", "Puntuable 100"));
        modos.add(new Modo(R.drawable.tablero_1, R.drawable.modo_1, "Aprende con lecciones", ""));
        modos.add(new Modo(R.drawable.tablero_1, R.drawable.modo_1, "Problema Diario", "19 feb.2021"));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_jugar, container, false);

        recyclerView = v.findViewById(R.id.recyclerJugar);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterJugar adapterJugar = new AdapterJugar(modos);
        recyclerView.setAdapter(adapterJugar);

        return v;


    }
}
