package cat.itb.chessclone;

public class Modo {
    private int tablero;
    private int imagenModo;
    private String nombre;
    private String descripcion;

    public Modo(int tablero, int imagenModo, String nombre, String descripcion) {
        this.tablero = tablero;
        this.imagenModo = imagenModo;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public int getTablero() {
        return tablero;
    }

    public void setTablero(int tablero) {
        this.tablero = tablero;
    }

    public int getImagenModo() {
        return imagenModo;
    }

    public void setImagenModo(int imagenModo) {
        this.imagenModo = imagenModo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
