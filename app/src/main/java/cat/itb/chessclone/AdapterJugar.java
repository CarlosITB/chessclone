package cat.itb.chessclone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterJugar extends RecyclerView.Adapter<AdapterJugar.ViewHolder> {
    List<Modo> modos;

    public AdapterJugar(List<Modo> modos) {
        this.modos = modos;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageTablero;
        ImageView imageModo;
        TextView textModo;
        TextView textDescription;

        public ViewHolder(@NonNull View view) {
            super(view);

            imageTablero = view.findViewById(R.id.tableroImage);
            imageModo = view.findViewById(R.id.modeImage);
            textModo = view.findViewById(R.id.modeText);
            textDescription = view.findViewById(R.id.detailsText);
        }

        public void bindData(Modo modo) {
            textModo.setText(modo.getNombre());
            textDescription.setText(modo.getDescripcion());
            imageTablero.setImageResource(modo.getTablero());
            imageModo.setImageResource(modo.getImagenModo());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_view, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(modos.get(position));
    }

    @Override
    public int getItemCount() {
        return modos.size();
    }
}



